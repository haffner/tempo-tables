# TEMPO tables

## TEMPO instrument tables for V8.6 TOMS total ozone algorithm

- TOMS V8.6 tables with VZA extended to 88 degrees.
- N-value master tables (monochromatic) were produced with TOMRAD 2.25; TOMRAD 2.22dndx used for dndx table.
- Cross-sections: O3 is BDM, Rayleigh is Bodhaine, O2O2 is Greenblatt.
- Solar flux: SOLSTICE
- Slit function: triangular FWHM=0.6 nm.
- Code used to generate tables is OMTO3LUT/vza88@38

Dave Haffner
2/14/2020
